#!/usr/bin/env python
from cdft.tasks.run import run_task
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


def main():
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('configfile', help='filename for config file')
    args = parser.parse_args()

    run_task(args.configfile)


if __name__ == '__main__':
    main()
