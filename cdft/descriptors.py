from __future__ import annotations

import warnings
from abc import ABC, abstractmethod
from itertools import product
from typing import List, Tuple, Optional, Dict, Sequence

import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression

model_suffix = {'PP': 'PP',
                'one parabola': 'PP',
                'Parr-Pearson': 'PP',
                '2P': '2P',
                'two parabola': '2P',
                'two parabola Chamorro': '2PCh',
                'two parabola Chamorro Aprox': '2PChA',
                'GQ': 'GQ',
                'GQM': 'GQ',
                'Generalized Quadratic': 'GQ',
                'Generalized Quadratic Model': 'GQ'}


class CDFTModel(ABC):
    def __init__(self, atoms):
        self.atoms = atoms
        self.I = atoms.I
        self.A = atoms.A


class DescriptorsModel(CDFTModel, ABC):
    def __init__(self, atoms):
        """
        Model to calculate chemical reactivity descriptors
        :param atoms: AtomsCT object from a charge transfer calculation
        """
        self.atoms = atoms
        self.name = atoms.name
        self.I = atoms.I
        self.A = atoms.A
        self.fukui_plus = atoms.fukui_plus
        self.fukui_minus = atoms.fukui_minus
        self.fukui_zero = (self.fukui_plus + self.fukui_minus) / 2
        self.nl = atoms.neighbor_list

    def get_atoms_of_interest(self, n: int, fukui: int = 0, method: str = 'fukui'):
        if method == 'neighbor':
            ix = self.get_atoms_of_interest_neighbor(n, fukui)
        else:
            if method != 'fukui':
                warnings.warn(f'Selection method "{method}" not recognized, defaulting to "fukui" method.')
            ix = self.get_atoms_of_interest_fukui(n, fukui)
        return ix

    def get_atoms_of_interest_neighbor(self, n: int, fukui: int = 0):
        if fukui == 1:
            fs = self.fukui_plus
        elif fukui == -1:
            fs = self.fukui_minus
        else:
            fs = self.fukui_zero

        # get the index of the atom with the largest fukui
        i0 = fs.argsort()[-1]
        ix = [i0]
        ix_neigh = list()
        for _ in range(n - 1):
            # update the neighbor list
            ix_neigh.extend([nn for nn in self.atoms.get_neighbors(i0) if nn not in ix])
            # get the neighbor list index of the neighbor with the largest fukui
            j0 = np.argsort([fs[j] for j in ix_neigh])[-1]
            # pop the index of the neighbor with the largest fukui
            i0 = ix_neigh.pop(j0)
            # add the index of the largest fukui to the results
            ix.append(i0)

        return ix

    def get_atoms_of_interest_fukui(self, n: int, fukui: int = 0):
        if fukui == 1:
            fs = self.fukui_plus
        elif fukui == -1:
            fs = self.fukui_minus
        else:
            fs = self.fukui_zero
        ix = tuple(i[0] for i in sorted(enumerate(fs), key=lambda x: x[1], reverse=True))
        return ix[:n]

    def f_minus(self, i: int):
        return self.fukui_minus[i]

    def f_plus(self, i: int):
        return self.fukui_plus[i]

    def f_zero(self, i: int):
        return self.fukui_zero[i]

    def f_minus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Nucleophilic Chemical Potential
        """
        return self.f_minus(i) * self.f_minus(j)

    def f_plus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Electrophilic Chemical Potential
        """
        return self.f_plus(i) * self.f_plus(j)

    def f_zero_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Nucleophilic Chemical Potential
        """
        return (self.f_minus_nonlocal(i, j) + self.f_plus_nonlocal(i, j)) / 2

    @property
    @abstractmethod
    def mu(self) -> float:
        """
        Chemical Potential
        """
        ...

    @property
    def chem_potential(self) -> float:
        return self.mu

    def mu_minus(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.mu_minus_global
        elif j is None:
            return self.mu_minus_local(i)
        return self.mu_minus_nonlocal(i, j)

    def mu_plus(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.mu_plus_global
        elif j is None:
            return self.mu_plus_local(i)
        return self.mu_plus_nonlocal(i, j)

    def mu_zero(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.mu_zero_global
        elif j is None:
            return self.mu_zero_local(i)
        return self.mu_zero_nonlocal(i, j)

    @property
    def mu_minus_global(self) -> float:
        return self.mu

    @property
    def mu_plus_global(self) -> float:
        return self.mu

    @property
    def mu_zero_global(self) -> float:
        return self.mu

    # TODO: check equations
    def mu_minus_local(self, i: int) -> float:
        """
        Local Nucleophilic Chemical Potential
        """
        return self.f_minus(i) * self.mu_minus_global

    def mu_plus_local(self, i: int) -> float:
        """
        Local Electrophilic Chemical Potential
        """
        return self.f_plus(i) * self.mu_plus_global

    def mu_zero_local(self, i: int) -> float:
        """
        Mean Local Chemical Potential
        """
        return (self.A * self.f_plus(i) + self.I * self.f_minus(i)) / 2

    def mu_minus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Nucleophilic Chemical Potential
        """
        return self.f_minus_nonlocal(i, j) * self.mu_minus_global

    def mu_plus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Electrophilic Chemical Potential
        """
        return self.f_plus_nonlocal(i, j) * self.mu_plus_global

    def mu_zero_nonlocal(self, i: int, j: int) -> float:
        """
        Mean Non-local Chemical Potential
        """
        return (self.mu_minus_nonlocal(i, j) + self.mu_plus_nonlocal(i, j)) / 2

    @property
    @abstractmethod
    def eta(self) -> float:
        """
        Chemical Hardness
        """
        ...

    @property
    def chem_hardness(self):
        return self.eta

    def eta_minus(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.eta_minus_global
        elif j is None:
            return self.eta_minus_local(i)
        return self.eta_minus_nonlocal(i, j)

    def eta_plus(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.eta_plus_global
        elif j is None:
            return self.eta_plus_local(i)
        return self.eta_plus_nonlocal(i, j)

    def eta_zero(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.eta_zero_global
        elif j is None:
            return self.eta_zero_local(i)
        return self.eta_zero_nonlocal(i, j)

    @property
    def eta_minus_global(self) -> float:
        return self.eta

    @property
    def eta_plus_global(self) -> float:
        return self.eta

    @property
    def eta_zero_global(self) -> float:
        return self.eta

    def eta_minus_local(self, i: int) -> float:
        """
        Local Nucleophilic Chemical Hardness
        """
        return self.eta * self.f_minus(i)

    def eta_plus_local(self, i: int) -> float:
        """
        Local Electrophilic Chemical Hardness
        """
        return self.eta * self.f_plus(i)

    def eta_zero_local(self, i: int) -> float:
        """
        Mean Local Chemical Hardness
        """
        return self.eta * self.f_zero(i)

    def eta_minus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Nucleophilic Chemical Hardness
        """
        return self.eta * self.f_minus_nonlocal(i, j)

    def eta_plus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Electrophilic Chemical Hardness
        """
        return self.eta * self.f_plus_nonlocal(i, j)

    def eta_zero_nonlocal(self, i: int, j: int) -> float:
        """
        Mean Non-local Chemical Hardness
        """
        return self.eta * self.f_zero_nonlocal(i, j)

    def etact_minus(self, i: int, j: int or None = None):
        if j is None:
            return self.etact_minus_local(i)
        return self.etact_minus_nonlocal(i, j)

    def etact_plus(self, i: int, j: int or None = None):
        if j is None:
            return self.etact_plus_local(i)
        return self.etact_plus_nonlocal(i, j)

    def etact_zero(self, i: int, j: int or None = None):
        if j is None:
            return self.etact_zero_local(i)
        return self.etact_zero_nonlocal(i, j)

    # TODO: ask for reference of these formulae. Gazquez
    def etact_minus_local(self, i: int) -> float:
        """
        Local Nucleophilic Chemical Hardness. Gazquez
        """
        return self.etact_zero_local(i) - self.eta * self.df(i)

    def etact_plus_local(self, i: int) -> float:
        """
        Local Electrophilic Chemical Hardness. Gazquez
        """
        return self.etact_zero_local(i) + self.eta * self.df(i)

    def etact_zero_local(self, i: int) -> float:
        """
        Mean Local Chemical Hardness. Gazquez
        """
        return self.I * self.f_minus(i) - self.A * self.f_plus(i)

    def etact_minus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Nucleophilic Chemical Hardness. Gazquez
        """
        return (self.eta * self.f_minus(i) * self.f_minus(j) -
                self.I * (self.df(i) * self.f_minus(j) + self.df(j) * self.f_minus(i)))

    def etact_plus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Electrophilic Chemical Hardness. Gazquez
        """
        return (self.eta * self.f_plus(i) * self.f_plus(j) -
                self.A * (self.df(i) * self.f_plus(j) + self.df(j) * self.f_plus(i)))

    def etact_zero_nonlocal(self, i: int, j: int) -> float:
        """
        Mean Non-local Chemical Hardness. Gazquez
        """
        return (self.eta * self.f_zero(i) * self.f_zero(j) -
                self.mu * (self.df(i) * self.f_zero(j) + self.df(j) * self.f_zero(i)))

    @property
    def omega(self):
        """
        Electrophilicity
        """
        return (self.mu ** 2) / (2 * self.eta)

    @property
    def omega_plus(self):
        """
        Electroaccepting power
        """
        return (self.mu_plus_global ** 2) / (2 * self.eta)

    @property
    def omega_minus(self):
        """
        Electrodonating power
        """
        return (self.mu_minus_global ** 2) / (2 * self.eta)

    @property
    def omega_plus_uf(self):
        """
        Electrophilicity as proposed by Miranda and Vela at UF
        """
        return (self.mu_plus_global) ** 2 / (2 * self.eta)

    def omega_minus_uf(self, mu_ref: float):
        """
        Nucleophilicity as proposed by Miranda and Vela at UF
        """
        return (self.mu_minus_global - mu_ref) ** 2 / (2 * self.eta)

    def omega_local(self, i: int) -> float:
        """
        Local Electropilicity
        """
        return self.omega * self.f_minus(i)

    def omega_minus_local(self, i: int) -> float:
        """
        Local Electrodonating power
        """
        return self.omega_minus * self.f_minus(i)

    def omega_plus_local(self, i: int) -> float:
        """
        Local Electroaccepting power
        """
        return self.omega_plus * self.f_plus(i)

    @property
    def s(self) -> float:
        """
        Chemical Softness
        """
        return 1 / self.eta

    @property
    def chem_softness(self):
        return self.s

    def s_minus(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.s_minus_global
        elif j is None:
            return self.s_minus_local(i)
        return self.s_minus_nonlocal(i, j)

    def s_plus(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.s_plus_global
        elif j is None:
            return self.s_plus_local(i)
        return self.s_plus_nonlocal(i, j)

    def s_zero(self, i: int or None = None, j: int or None = None):
        if i is None:
            if j is not None:
                raise ValueError('Unexpected value for `j` when `i` is None.')
            return self.s_zero_global
        elif j is None:
            return self.s_zero_local(i)
        return self.s_zero_nonlocal(i, j)

    @property
    def s_minus_global(self) -> float:
        return self.s

    @property
    def s_plus_global(self) -> float:
        return self.s

    @property
    def s_zero_global(self) -> float:
        return self.s

    def s_minus_local(self, i: int) -> float:
        """
        Local Nucleophilic Chemical Hardness
        """
        return self.s * self.f_minus(i)

    def s_plus_local(self, i: int) -> float:
        """
        Local Electrophilic Chemical Hardness
        """
        return self.s * self.f_plus(i)

    def s_zero_local(self, i: int) -> float:
        """
        Mean Local Chemical Hardness
        """
        return self.s * self.f_zero(i)

    def s_minus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Nucleophilic Chemical Hardness
        """
        return self.s * self.f_minus_nonlocal(i, j)

    def s_plus_nonlocal(self, i: int, j: int) -> float:
        """
        Non-local Electrophilic Chemical Hardness
        """
        return self.s * self.f_plus_nonlocal(i, j)

    def s_zero_nonlocal(self, i: int, j: int) -> float:
        """
        Mean Non-local Chemical Hardness
        """
        return self.s * self.f_zero_nonlocal(i, j)

    def Af_plus(self, i: int):
        return self.A * self.f_plus(i)

    def Af_minus(self, i: int):
        return self.A * self.f_minus(i)

    def df(self, i: int):
        return self.f_plus(i) - self.f_minus(i)

    def df_nonlocal(self, i: int, j: int):
        return self.f_plus_nonlocal(i, j) - self.f_minus_nonlocal(i, j)

    def __len__(self):
        return len(self.fukui_plus)


class OneParabola(DescriptorsModel):
    @property
    def mu(self) -> float:
        """
        Chemical Potential
        """
        return -(self.I + self.A) / 2

    @property
    def eta(self) -> float:
        """
        Chemical Hardness
        """
        return self.I - self.A

    @property
    def etact(self, i) -> float:
        raise NotImplementedError("Use `mu_plus` or `mu_minus` instead.")


class TwoParabola(DescriptorsModel):
    @property
    def mu(self):
        """
        Chemical Potential
        """
        raise NotImplementedError("Use `mu_plus` or `mu_minus` instead.")

    @property
    def mu_plus_global(self) -> float:
        """
        Electrophilic Chemical Potential
        """
        # TODO: check name is correct
        return -(self.I + 3 * self.A) / 4

    @property
    def mu_minus_global(self) -> float:
        """
        Nucleophilic Chemical Potential
        """
        return -(3 * self.I + self.A) / 4

    @property
    def mu_zero_global(self) -> float:
        """
        Mean Chemical Potential
        """
        return (self.mu_minus_global + self.mu_plus_global) / 2

    @property
    def eta(self) -> float:
        """
        Chemical Hardness
        """
        return (self.I - self.A) / 2

    @property
    def omega(self):
        """
        Electrophilicity
        """
        raise NotImplementedError("Use `omega_plus` or `omega_minus` instead.")


class TwoParabolaCh(TwoParabola):
    """
    Chamorro's Two-Parabola Approximation model

    Model III from "E. Chamorro, M. Duque-Noreña, P. Pérez, J. Mol. Struct. (THEOCHEM) 896 (2009) 73."

    """

    @property
    def mu_plus_global(self) -> float:
        """
        Electrophilic Chemical Potential
        """
        return -self.A

    @property
    def mu_minus_global(self) -> float:
        """
        Nucleophilic Chemical Potential
        """
        return -self.I


class DescriptorsModelTruncated(CDFTModel):
    ...


class TwoParabolaChApproximation(DescriptorsModelTruncated):
    """
    Model IV from "E. Chamorro, M. Duque-Noreña, P. Pérez, J. Mol. Struct. (THEOCHEM) 896 (2009) 73."
    """

    @property
    def omega_plus(self):
        return self.A ** 2 / (2 * abs(self.I))

    @property
    def omega_minus(self):
        return self.I ** 2 / (2 * abs(self.A))


class GeneralizedQuadratic(DescriptorsModel):
    def __init__(self, atoms: AtomsCT):
        super().__init__(atoms)
        self.HOMO = atoms.HOMO
        self.LUMO = atoms.LUMO

    @property
    def mu(self):
        """
        Chemical Potential
        """
        raise NotImplementedError("Use `mu_plus` or `mu_minus` instead.")

    @property
    def mu_plus_global(self) -> float:
        """
        Electrophilic Chemical Potential
        """
        return self.LUMO

    @property
    def mu_minus_global(self) -> float:
        """
        Nucleophilic Chemical Potential
        """
        return self.HOMO

    @property
    def mu_zero_global(self) -> float:
        """
        Mean Chemical Potential
        """
        return (self.mu_minus_global + self.mu_plus_global) / 2

    @property
    def eta(self) -> float:
        """
        Chemical Hardness
        """
        raise NotImplementedError("Use `eta_plus` or `eta_minus` instead.")

    @property
    def eta_minus_global(self) -> float:
        return 2 * (self.I + self.HOMO)

    @property
    def eta_plus_global(self) -> float:
        return -2 * (self.A + self.LUMO)

    @property
    def omega(self):
        """
        Electrophilicity
        """
        raise NotImplementedError("Use `omega_plus` or `omega_minus` instead.")

    @property
    def omega_plus(self):
        """
        Electroaccepting power
        """
        return (self.mu_plus_global ** 2) / (2 * self.eta_plus_global)

    @property
    def omega_minus(self):
        """
        Electrodonating power
        """
        return (self.mu_minus_global ** 2) / (2 * self.eta_minus_global)


class ChargeTransfer:
    _ff: str = '8.4f'

    # TODO: reformat
    def __init__(self, dsA: DescriptorsModel, dsB: DescriptorsModel):
        self.name = dsA.name
        self._f_name = str(len(self.name))
        self.A = dsA
        self.B = dsB

    @property
    @abstractmethod
    def dNA_ele_global(self):
        """Electrophilic charge transfer to A"""
        ...

    @property
    def dNB_nuc_global(self):
        """Nucleophilic charge transfer to B"""
        return -self.dNA_ele_global

    @property
    def dNB_ele_global(self):
        """Electrophilic charge transfer to B"""
        return -self.dNA_nuc_global

    @property
    @abstractmethod
    def dNA_nuc_global(self):
        """Nucleophilic charge transfer to A"""
        ...

    @property
    def dNA_global(self):
        """Charge transfer to A"""
        return self.dNA_ele_global + self.dNA_nuc_global

    @property
    def dNB_global(self):
        """Charge transfer to B"""
        return self.dNB_ele_global + self.dNB_nuc_global

    @abstractmethod
    def dNA_nuc_local(self, setA, setB):
        ...

    def dNB_ele_local(self, setA, setB):
        """Electrophilic charge transfer to B"""
        return -self.dNA_nuc_local(setA, setB)

    @abstractmethod
    def dNA_ele_local(self, setA, setB):
        ...

    def dNB_nuc_local(self, setA, setB):
        """Electrophilic charge transfer to B"""
        return -self.dNA_ele_local(setA, setB)

    def dNA_local(self, setA, setB):
        """Charge transfer to A"""
        return self.dNA_ele_local(setA, setB) + self.dNA_nuc_local(setA, setB)

    def dNB_local(self, setA, setB):
        """Charge transfer to B"""
        return self.dNB_ele_local(setA, setB) + self.dNB_nuc_local(setA, setB)


class ChargeTransferPP(ChargeTransfer):
    def __init__(self, dsA: OneParabola, dsB: OneParabola):
        super().__init__(dsA, dsB)

    # Global Charge Transfer Model
    @property
    def dNA_ele_global(self):
        """Electrophilic charge transfer to A"""
        return -(self.B.A - self.A.I) / (2 * (self.B.eta + self.A.eta))

    @property
    def dNA_nuc_global(self):
        """Nucleophilic charge transfer to A"""
        return (self.A.A - self.B.I) / (2 * (self.B.eta + self.A.eta))

    # Local Charge Transfer Model
    def dNA_ele_local(self, setA, setB):
        """Electrophilic charge transfer to A"""
        fb_minus = sum(self.B.f_minus(b) for b in setB)
        fa_plus = sum(self.A.f_plus(a) for a in setA)
        return -(self.B.A * fb_minus - self.A.I * fa_plus) / (2 * (self.B.eta * fb_minus + self.A.eta * fa_plus))

    def dNA_nuc_local(self, setA, setB):
        """Nucleophilic charge transfer to A"""
        fa_minus = sum(self.A.f_minus(a) for a in setA)
        fb_plus = sum(self.B.f_plus(b) for b in setB)
        return (fa_minus * self.A.A - fb_plus * self.B.I) / (2 * (fb_plus * self.B.eta + fa_minus * self.A.eta))

    @staticmethod
    def _str_header_global(species: str = 'A', logk: Optional[float] = None):
        text = f'   I_{species}    |   A_{species}    |   μ_{species}    |   η_{species}    '
        if species == 'A':
            text += f'|   ΔNA    |  ΔNA_nuc |  ΔNA_ele'
            if logk is not None:
                text += ' |   logk'
        text += '\n'
        return text

    @staticmethod
    def _str_header_local(species: str = 'A', logk: Optional[float] = None):
        text = f'   Σf{species}-  |   Σf{species}+'
        if species == 'A':
            text += f'  |   ΔNA     |  ΔNA_nuc |  ΔNA_ele'
            if logk is not None:
                text += ' |   logk'
        text += '\n'
        return text

    def str_A_global(self, logk: Optional[float] = None):
        ff = self._ff
        text = f' {self.A.I:{ff}} | {self.A.A:{ff}} | '
        text += f'{self.A.mu:{ff}} | {self.A.eta:{ff}} | '
        text += f'{self.dNA_global:{ff}} | {self.dNA_nuc_global:{ff}} | {self.dNA_ele_global:{ff}} | '
        if logk is not None:
            text += f'  {logk:{ff}}'
        text += '\n'
        return text

    def str_B_global(self):
        ff = self._ff
        text = f' {self.B.I:{ff}} | {self.B.A:{ff}} | '
        text += f'{self.B.mu:{ff}} | {self.B.eta:{ff}}\n'
        return text

    def str_A_local(self, ix_A, ix_B, logk: Optional[float] = None):
        ff = self._ff

        fukui_ele = sum(self.A.f_minus(i) for i in ix_A)
        dNA_ele = self.dNA_ele_local(ix_A, ix_B)
        fukui_nuc = sum(self.A.f_plus(i) for i in ix_A)
        dNA_nuc = self.dNA_nuc_local(ix_A, ix_B)

        text = f'{fukui_nuc:{ff}} | {fukui_ele:{ff}} | '
        text += f'{self.dNA_local(ix_A, ix_B):{ff}} | '
        text += f'{dNA_nuc:{ff}} | {dNA_ele:{ff}}'
        if logk is not None:
            text += f' | {logk:{ff}}'
        text += '\n'
        return text

    def str_B_local(self, ix_B):
        fukui_minus = sum(self.B.f_minus(i) for i in ix_B)
        fukui_plus = sum(self.B.f_plus(i) for i in ix_B)
        text = f'{fukui_minus:{self._ff}} | {fukui_plus:{self._ff}}\n'
        return text

    def str_report(self, setA: Sequence[int], setB: Sequence[int], header: bool = True, include_B: bool = True,
                   logk: Optional[float] = None):

        # Global Charge Transfer
        if header:
            text = 'Global Charge Transfer\n'
            text += self._str_header_global('A', logk)
        text += self.str_A_global(logk)
        if include_B:
            if header:
                text += self._str_header_global('B')
            text += self.str_B_global()

        # Global Charge Transfer
        if header:
            text += f'\nLocal Charge Transfer {len(setA)}-{len(setB)}\n'
            text += self._str_header_local('A', logk)
        text += self.str_A_local(setA, setB, logk)
        if include_B:
            if header:
                text += self._str_header_local('B')
            text += self.str_B_local(setB)

        # fukui and atoms of interest
        selected = ['*' if i in setA else ' ' for i in range(len(self.A.atoms))]
        text += '\nAtom_A fukui+   fukui- selected\n'
        text += '\n'.join(f'  {a.symbol:<2} {fp:{self._ff}} {fm:{self._ff}}    {s}'
                          for a, fp, fm, s in zip(self.A.atoms, self.A.fukui_plus, self.A.fukui_minus, selected))
        text += f"Active atoms' index: {' '.join(str(i) for i in setA)}\n"

        selected = ['*' if i in setB else ' ' for i in range(len(self.B.atoms))]
        text += '\nAtom_B fukui+   fukui- selected\n'
        text += '\n'.join(f'  {a.symbol:<2} {fp:{self._ff}} {fm:{self._ff}}    {s}'
                          for a, fp, fm, s in zip(self.B.atoms, self.B.fukui_plus, self.B.fukui_minus, selected))
        text += f"Active atoms' index: {' '.join(str(i) for i in setB)}\n"

        return text

    def report(self, ct_type, a=None, b=None, local_selection='fukui', logk: Optional[float] = None):
        if a is None or b is None:
            # TODO: add only global report
            pass
        else:
            fukui = 1 if ct_type == 'ele' else -1
            setA = self.A.get_atoms_of_interest(a, fukui, local_selection)
            setB = self.B.get_atoms_of_interest(b, fukui, local_selection)
            text = self.str_report(setA, setB, logk=logk)
        return text


class ChargeTransfer2P(ChargeTransfer):
    def __init__(self, dsA: TwoParabola, dsB: TwoParabola):
        super().__init__(dsA, dsB)

    # Global Charge Transfer Model
    @property
    def dNA_ele_global(self):
        """Electrophilic charge transfer to A"""
        return (self.B.mu_minus_global - self.A.mu_plus_global) / (self.A.eta + self.B.eta)

    @property
    def dNA_nuc_global(self):
        """Nucleophilic charge transfer to A"""
        return (self.B.mu_plus_global - self.A.mu_minus_global) / (self.A.eta + self.B.eta)

    # Local Charge Transfer Model
    def dNA_ele_local(self, setA, setB):
        """Electrophilic charge transfer to A"""
        fb_minus = sum(self.B.f_minus(b) for b in setB)
        fa_plus = sum(self.A.f_plus(a) for a in setA)
        # TODO: check equations
        return (self.B.mu_minus_global * fb_minus - self.A.mu_plus_global * fa_plus) / (
                self.B.eta * fb_minus + self.A.eta * fa_plus)

    def dNA_nuc_local(self, setA, setB):
        """Nucleophilic charge transfer to A"""
        fa_minus = sum(self.A.f_minus(a) for a in setA)
        fb_plus = sum(self.B.f_plus(b) for b in setB)
        return (fb_plus * self.B.mu_plus_global - fa_minus * self.A.mu_minus_global) / (
                fb_plus * self.B.eta + fa_minus * self.A.eta)

    @staticmethod
    def _str_header_global(species: str = 'A', logk: Optional[float] = None):
        text = f'   I_{species}    |   A_{species}    |   μ+_{species}   |   μ-_{species}   |   η_{species}    '
        if species == 'A':
            text += f'|   ΔNA    |  ΔNA_nuc |  ΔNA_ele'
        if logk is not None:
            text += f' |   log k'
        text += '\n'
        return text

    @staticmethod
    def _str_header_local(species: str = 'A', logk: Optional[float] = None):
        text = f'   Σf{species}-  |    Σf{species}-'
        if species == 'A':
            text += f'  |   ΔNA    |  ΔNA_nuc |  ΔNA_ele'
            if logk is not None:
                text += f' |   logk'
        text += '\n'
        return text

    def str_A_global(self, logk: Optional[float] = None):
        ff = self._ff
        text = f' {self.A.I:{ff}} | {self.A.A:{ff}} | '
        text += f'{self.A.mu_plus_global:{ff}} | {self.A.mu_minus_global:{ff}} | {self.A.eta:{ff}} | '
        text += f'{self.dNA_global:{ff}} | {self.dNA_nuc_global:{ff}} | {self.dNA_ele_global:{ff}}'
        if logk is not None:
            text += f' | {logk:{ff}}'
        text += '\n'
        return text

    def str_B_global(self):
        ff = self._ff
        text = f' {self.B.I:{ff}} | {self.B.A:{ff}} | '
        text += f'{self.B.mu_plus_global:{ff}} | {self.B.mu_minus_global:{ff}} | {self.B.eta:{ff}}\n'
        return text

    def str_A_local(self, ix_A, ix_B, logk: Optional[float] = None):
        ff = self._ff
        fukui_minus = sum(self.A.f_minus(i) for i in ix_A)
        dNA_minus = self.dNA_ele_local(ix_A, ix_B)
        fukui_plus = sum(self.A.f_plus(i) for i in ix_A)
        dNA_plus = self.dNA_nuc_local(ix_A, ix_B)

        text = f'{fukui_minus:{ff}} | {fukui_plus:{ff}} | '
        text += f'{self.dNA_local(ix_A, ix_B):{ff}} | {dNA_plus:{ff}} | {dNA_minus:{ff}}'
        if logk is not None:
            text += f' | {logk:{ff}}'
        text += '\n'
        return text

    def str_B_local(self, ix_B):
        fukui_minus = sum(self.B.f_minus(i) for i in ix_B)
        fukui_plus = sum(self.B.f_plus(i) for i in ix_B)
        text = f'{fukui_minus:{self._ff}} | {fukui_plus:{self._ff}}\n'
        return text

    def str_report(self, setA: Sequence[int], setB: Sequence[int], header: bool = True, include_B: bool = True,
                   logk: Optional[float] = None):

        # Global Charge Transfer
        if header:
            text = 'Global Charge Transfer\n'
            text += self._str_header_global('A')
        text += self.str_A_global(logk)
        if include_B:
            if header:
                text += self._str_header_global('B')
            text += self.str_B_global()

        # Global Charge Transfer
        if header:
            text += f'\nLocal Charge Transfer {len(setA)}-{len(setB)}\n'
            text += self._str_header_local('A', logk)
        text += self.str_A_local(setA, setB, logk)
        if include_B:
            if header:
                text += self._str_header_local('B')
            text += self.str_B_local(setB)

        # fukui and atoms of interest
        selected = ['*' if i in setA else ' ' for i in range(len(self.A.atoms))]
        text += '\nAtom_A fukui+   fukui- selected\n'
        text += '\n'.join(f'  {a.symbol:<2} {fp:{self._ff}} {fm:{self._ff}}    {s}'
                          for a, fp, fm, s in zip(self.A.atoms, self.A.fukui_plus, self.A.fukui_minus, selected))
        text += f"Active atoms' index: {' '.join(str(i) for i in setA)}\n"

        selected = ['*' if i in setB else ' ' for i in range(len(self.B.atoms))]
        text += '\nAtom_B fukui+   fukui- selected\n'
        text += '\n'.join(f'  {a.symbol:<2} {fp:{self._ff}} {fm:{self._ff}}    {s}'
                          for a, fp, fm, s in zip(self.B.atoms, self.B.fukui_plus, self.B.fukui_minus, selected))
        text += f"Active atoms' index: {' '.join(str(i) for i in setB)}\n"

        return text

    def report(self, ct_type, a=None, b=None, local_selection='fukui', logk: Optional[float] = None):
        if a is None or b is None:
            # TODO: add only global report
            pass
        else:
            fukui = 1 if ct_type == 'ele' else -1
            setA = self.A.get_atoms_of_interest(a, fukui, local_selection)
            setB = self.B.get_atoms_of_interest(b, fukui, local_selection)
            text = self.str_report(setA, setB, logk=logk)
        return text


class ChargeTransferGQ(ChargeTransfer):
    def __init__(self, dsA: GeneralizedQuadratic, dsB: GeneralizedQuadratic):
        super().__init__(dsA, dsB)

    # Global Charge Transfer Model
    @property
    def dNA_ele_global(self):
        """Electrophilic charge transfer to A"""
        return (self.B.mu_minus_global - self.A.mu_plus_global) / (self.B.eta_minus_global + self.A.eta_plus_global)

    @property
    def dNA_nuc_global(self):
        """Nucleophilic charge transfer to A"""
        return (self.B.mu_plus_global - self.A.mu_minus_global) / (self.A.eta_minus_global + self.B.eta_plus_global)

    # Local Charge Transfer Model
    def dNA_ele_local(self, setA, setB):
        """Electrophilic charge transfer to A"""
        fa_plus = sum(self.A.f_plus(a) for a in setA)
        fb_minus = sum(self.B.f_minus(b) for b in setB)
        return (self.B.mu_minus_global * fb_minus - self.A.mu_plus_global * fa_plus) / (
                self.B.eta_minus_global * fb_minus + self.A.eta_plus_global * fa_plus)

    def dNA_nuc_local(self, setA, setB):
        """Nucleophilic charge transfer to A"""
        fa_minus = sum(self.A.f_minus(a) for a in setA)
        fb_plus = sum(self.B.f_plus(b) for b in setB)
        return (fb_plus * self.B.mu_plus_global - fa_minus * self.A.mu_minus_global) / (
                fb_plus * self.B.eta_plus_global + fa_minus * self.A.eta_minus_global)

    @staticmethod
    def _str_header_global(species: str = 'A', logk: Optional[float] = None) -> str:
        text = f'   I_{species}    |   A_{species}    |   μ+_{species}   |   μ-_{species}   |   η+_{species}   |   η-_{species}'
        if species == 'A':
            text += f'   |   ΔNA    |  ΔNA_nuc |  ΔNA_ele'
        if logk is not None:
            text += f' |   logk'
        text += '\n'
        return text

    @staticmethod
    def _str_header_local(species: str = 'A', logk: Optional[float] = None) -> str:
        text = f'   Σf{species}-  |    Σf{species}+'
        if species == 'A':
            text += f'  |   ΔNA    |  ΔNA_nuc |  ΔNA_nuc'
        if logk is not None:
            text += f' |   logk'
        text += '\n'
        return text

    def str_A_global(self, logk: Optional[float] = None) -> str:
        ff = self._ff
        text = f' {self.A.I:{ff}} | {self.A.A:{ff}} | '
        text += f'{self.A.mu_plus_global:{ff}} | {self.A.mu_minus_global:{ff}} | {self.A.eta_plus_global:{ff}} | '
        text += f'{self.A.eta_minus_global:{ff}} | {self.dNA_global:{ff}} | '
        text += f'{self.dNB_nuc_global:{ff}} | {self.dNB_ele_global:{ff}}'
        if logk is not None:
            text += f' | {logk:{ff}}'
        text += '\n'
        return text

    def str_B_global(self):
        ff = self._ff
        text = f' {self.B.I:{ff}} | {self.B.A:{ff}} | '
        text += f'{self.B.mu_plus_global:{ff}} | {self.B.mu_minus_global:{ff}} | {self.B.eta_plus_global:{ff}} | '
        text += f'{self.B.eta_minus_global:{ff}}\n'
        return text

    def str_A_local(self, ix_A, ix_B, logk: Optional[float] = None) -> str:
        ff = self._ff

        fukui_minus = sum(self.A.f_minus(i) for i in ix_A)
        dNA_ele = self.dNA_ele_local(ix_A, ix_B)
        fukui_plus = sum(self.A.f_plus(i) for i in ix_A)
        dNA_nuc = self.dNA_nuc_local(ix_A, ix_B)

        text = f'{fukui_minus:{ff}} | {fukui_plus:{ff}} | '
        text += f'{self.dNA_local(ix_A, ix_B):{ff}} | '
        text += f'{dNA_nuc:{ff}} | {dNA_ele:{ff}}'
        if logk is not None:
            text += f' | {logk:{ff}}'
        text += f'\n'
        return text

    def str_B_local(self, ix_B):
        fukui_minus = sum(self.B.f_minus(i) for i in ix_B)
        fukui_plus = sum(self.B.f_plus(i) for i in ix_B)
        text = f'{fukui_minus:{self._ff}} | {fukui_plus:{self._ff}}\n'
        return text

    def str_report(self, setA: Sequence[int], setB: Sequence[int], header: bool = True, include_B: bool = True,
                   logk: Optional[int] = None) -> str:

        # Global Charge Transfer
        if header:
            text = 'Global Charge Transfer\n'
            text += self._str_header_global('A', logk)
        text += self.str_A_global()
        if include_B:
            if header:
                text += self._str_header_global('B')
            text += self.str_B_global()

        # Global Charge Transfer
        if header:
            text += f'\nLocal Charge Transfer {len(setA)}-{len(setB)}\n'
            text += self._str_header_local('A', logk)
        text += self.str_A_local(setA, setB, logk=logk)
        if include_B:
            if header:
                text += self._str_header_local('B')
            text += self.str_B_local(setB)

        # fukui and atoms of interest
        selected = ['*' if i in setA else ' ' for i in range(len(self.A.atoms))]
        text += '\nAtom_A fukui+   fukui- selected\n'
        text += '\n'.join(f'  {a.symbol:<2} {fp:{self._ff}} {fm:{self._ff}}    {s}'
                          for a, fp, fm, s in zip(self.A.atoms, self.A.fukui_plus, self.A.fukui_minus, selected))
        text += f"Active atoms' index: {' '.join(str(i) for i in setA)}\n"

        selected = ['*' if i in setB else ' ' for i in range(len(self.B.atoms))]
        text += '\nAtom_B fukui+   fukui- selected\n'
        text += '\n'.join(f'  {a.symbol:<2} {fp:{self._ff}} {fm:{self._ff}}    {s}'
                          for a, fp, fm, s in zip(self.B.atoms, self.B.fukui_plus, self.B.fukui_minus, selected))
        text += f"Active atoms' index: {' '.join(str(i) for i in setB)}\n"

        return text

    def report(self, ct_type, a=None, b=None, local_selection='fukui', logk: Optional[float] = None) -> str:
        # TODO: add HOMO and LUMO energies
        if a is None or b is None:
            # TODO: add only global report
            pass
        else:
            fukui = 1 if ct_type == 'ele' else -1
            setA = self.A.get_atoms_of_interest(a, fukui, local_selection)
            setB = self.B.get_atoms_of_interest(b, fukui, local_selection)
            text = self.str_report(setA, setB, logk=logk)
        return text


class ChargeTransferFamily:
    def __init__(self, cts: List[ChargeTransfer], exp_values: List[float],
                 data_mayr: Optional[Dict[str, List[float]]] = None):
        self.moleculesA = [ct.name for ct in cts]
        namesB = {ct.B.name for ct in cts}
        if len(namesB) != 1:
            raise ValueError(
                f'Expected all ChargeTransfer objects to share species B.\nGot the following speciesB: {namesB}')
        self.moleculeB = namesB.pop()
        self.reactants = (self.moleculesA, self.moleculeB)
        self.cts = cts
        self.exp_values: np.ndarray = np.array(exp_values)
        if data_mayr is not None:
            if len(data_mayr.get('N', [])) or len(data_mayr.get('E', [])):
                for k, v in data_mayr.items():
                    data_mayr[k] = np.array(v)
                self.data_mayr = data_mayr
            else:
                self.data_mayr = None
        else:
            self.data_mayr = None

    def dNA_global(self, ct_type: str):
        if ct_type not in ['ele', 'nuc']:
            raise ValueError(f"Expected ct_type to be one of ['nuc', 'ele']. Got '{ct_type}' instead.")
        if ct_type == 'ele':
            dNA = [ct.dNA_ele_global for ct in self.cts]
        else:
            dNA = [ct.dNA_nuc_global for ct in self.cts]
        return dNA

    def dNA_local(self, a: int, b: int, ct_type: str, local_selection='fukui'):
        if ct_type not in ['ele', 'nuc']:
            raise ValueError(f"Expected ct_type to be one of ['nuc', 'ele']. Got '{ct_type}' instead.")
        fukui = 1 if ct_type == 'ele' else -1
        ix_b = self.cts[0].B.get_atoms_of_interest(b, fukui, local_selection)
        if ct_type == 'ele':
            dNA = [ct.dNA_ele_local(ct.A.get_atoms_of_interest(a, fukui, local_selection), ix_b)
                   for ct in self.cts]
        else:
            dNA = [ct.dNA_nuc_local(ct.A.get_atoms_of_interest(a, fukui, local_selection), ix_b)
                   for ct in self.cts]
        return dNA

    def dNA(self):
        pass

    def correlation_global(self, ct_type: str):
        dNA = self.dNA_global(ct_type)
        x = np.array(dNA).reshape(-1, 1)
        if ct_type == 'nuc':
            x = abs(x)
        y = self.exp_values
        model = LinearRegression()
        model.fit(x, y)
        r_sq = model.score(x, y)
        intercept = model.intercept_
        slope = model.coef_[0]
        return r_sq, intercept, slope

    def correlations_local(self, ct_type: str, local_selection: str = 'fukui'):
        if ct_type not in ['ele', 'nuc']:
            raise ValueError(f"Expected ct_type to be one of ['nuc', 'ele']. Got '{ct_type}' instead.")

        na = min(len(ct.A) for ct in self.cts)
        nb = len(self.cts[0].B)
        y = self.exp_values

        correlations: Dict[Tuple[int, int], Tuple[float, float, float]] = dict()
        for group in product(range(1, na + 1), range(1, nb + 1)):
            a, b = group
            dNA = self.dNA_local(a, b, ct_type, local_selection)
            x = np.abs(np.array(dNA).reshape(-1, 1))
            # correlation
            # x = np.abs(x).reshape(-1, 1)
            model = LinearRegression()
            model.fit(x, y)
            r_sq = model.score(x, y)
            intercept = model.intercept_
            slope = model.coef_[0]
            correlations[group] = (r_sq, intercept, slope)
        return correlations

    def get_best_group(self, ct_type: str, local_selection: str = 'fukui') -> Tuple[
        Tuple[int, int], Tuple[float, float, float]]:
        correlations = self.correlations_local(ct_type, local_selection)
        return max(correlations.items(), key=lambda kv: kv[1][0])

    def plot_group(self, ct_type: str, local_selection: str = 'fukui', group: Optional[Tuple[int, int]] = None,
                   ax: Optional[plt.axes] = None, label: str = ''):
        if group:
            r_sq, intercept, slope = self.correlations_local(ct_type, local_selection)[group]
        else:
            group, (r_sq, intercept, slope) = self.get_best_group(ct_type, local_selection)

        a, b = group
        y = self.exp_values
        x = np.abs(np.array(self.dNA_local(a, b, ct_type, local_selection)))
        y_pred = intercept + slope * x

        # plot fit
        if ax is None:
            plt.figure()
            ax = plt.gca()
        # TODO: Check ME.
        ax.plot(x, y_pred, 'k', label=f'y = {slope:.4f}x + {intercept:.4f}')
        ax.scatter(x, y, label=f'$r^2 = {r_sq:.4f}$')
        if ct_type == 'nuc':
            txt_type = 'Nucleophilic'
        elif ct_type == 'ele':
            txt_type = 'Electrophilic'
        ax.set_title(f'{txt_type} charge transfer {a}-{b}')
        ax.set_xlabel(rf'$|\Delta N_A^{{{ct_type}}}|$')
        ax.set_ylabel('$log k$')
        ax.legend()
        plt.savefig(f'ctl_corr_{ct_type}{label}_{a}-{b}.png', dpi=300)
        plt.close()

    def plot_compare(self, ct_type: str, local_selection: str = 'fukui', ax: Optional[plt.axes] = None,
                     label: str = ''):
        correlations = self.correlations_local(ct_type, local_selection)

        na = min(len(ct.A) for ct in self.cts)
        nb = len(self.cts[0].B)

        if ax is None:
            plt.figure()
            ax = plt.gca()

        y = list()
        x_str = list()
        for a, b in product(range(1, na + 1), range(1, nb + 1)):
            group = (a, b)
            x_str.append(f'{a}-{b}')
            (corr, _, _) = correlations[group]
            y.append(corr)

        x = list(range(len(y)))
        corr_global, *_ = self.correlation_global(ct_type)
        ax.plot(x, y, marker='o', label=f'$|\Delta N_A^{{{ct_type}}}|$')
        ax.axhline(y=corr_global, color='g', linestyle=':', label=f'Global $r^2_{{{ct_type}}} = {corr_global:.4f}$')
        ax.set_xlabel('groups')
        ax.set_ylabel('$r^2$')
        ax.set_ylim([0, 1])
        ax.set_xticks(x, x_str, fontsize=10, rotation=90)
        ax.legend()
        plt.savefig(f'ctl_corr{label}.png', dpi=300)

        data = 'index group correlation\n'
        data += '\n'.join(f'{i} {name} {corr}'
                          for name, i, corr in zip(x_str, x, y))
        with open(f'data_correlations_groups{label}.dat', 'w') as f:
            print(data, file=f)

    def plot(self, ct_type: str, local_selection: str = 'fukui', label: str = ''):
        self.plot_global(ct_type, label=label)
        self.plot_group(ct_type, local_selection, label=label)
        self.plot_compare(ct_type, local_selection, label=label)

    def report_local(self, a: int, b: int, ct_type: str, local_selection='fukui'):
        if ct_type == 'ele':
            ct_type_str = 'Electrophilic'
        elif ct_type == 'nuc':
            ct_type_str = 'Nucleophilic'
        # Table title
        res = f'Local {ct_type_str} Charge Transfer {a}-{b}\n'
        # use max len of names for string format
        f_name = str(max([len(ct.name) for ct in self.cts] + [8]))
        # use the first CT to get the headers
        ct = self.cts[0]
        txtMolecule = f'\n{"Molecule":{f_name}} | '
        res += txtMolecule + ct._str_header_local('A', logk=True)

        fukui = 1 if ct_type == 'ele' else -1
        # get the atoms of interest for B
        ix_B = ct.B.get_atoms_of_interest(b, fukui, local_selection)
        # get the str report for each reactant
        for ct, logk in zip(self.cts, self.exp_values):
            ix_A = ct.A.get_atoms_of_interest(a, fukui, local_selection)
            res += f'{ct.name:{f_name}} | ' + ct.str_A_local(ix_A, ix_B, logk=logk)
        # report for B
        res += txtMolecule + ct._str_header_local('B')
        res += f'{self.moleculeB:{f_name}} | ' + ct.str_B_local(ix_B)
        return res

    def plot_global(self, ct_type: str, ax: Optional[plt.axes] = None, label: str = ''):
        if ax is None:
            plt.figure(figsize=(6, 6))
            ax = plt.gca()
        r_sq, intercept, slope = self.correlation_global(ct_type)
        x = np.array(self.dNA_global(ct_type))
        if ct_type == 'nuc':
            x = abs(x)
        y = self.exp_values
        y_pred = intercept + slope * x
        label_dna = f'\Delta N_A^{{{ct_type}}}'
        if ct_type == 'nuc':
            label_dna = f'|{label_dna}|'
        label_line = f'$\log k={slope:.2f}{label_dna}{intercept:+.2f}$'
        label_r2 = f'$R^2 = {r_sq:.3f}$'
        labeltxt = f'{label_line}    {label_r2}'
        ax.plot(x, y_pred, 'k', label=label_line)
        ax.scatter(x, y, 60, label=label_r2)
        # for x, y, ct in zip(x, y, self.cts):
        #     ax.annotate(ct.name,
        #                 (x, y),
        #                 (0, 10),
        #                 textcoords='offset points')

        if ct_type == 'nuc':
            txt_type = 'Nucleophilic'
        elif ct_type == 'ele':
            txt_type = 'Electrophilic'

        # TODO: add parameters from config
        props = dict(facecolor='white', edgecolor='white', boxstyle='round,pad=0.5')
        plt.gcf().text(0.5, 0.91, labeltxt, fontsize=12, ha='center', bbox=props)
        ax.tick_params(axis='both', which='both', direction='in', top=True, right=True)
        ax.tick_params(axis='both', which='major', length=10, width=1.5)
        ax.tick_params(axis='both', which='minor', length=5, width=1)
        ax.xaxis.set_label_position('bottom')
        ax.yaxis.set_label_position('left')
        ax.xaxis.set_major_formatter('{x:.2f}')
        ax.minorticks_on()

        # ax.set_title(f'Global {txt_type} Charge Transfer')
        ax.set_xlabel(f'${label_dna}$', fontsize=20, fontweight='bold')
        ax.set_ylabel('$\log k$', fontsize=20, fontweight='bold')
        # ax.set_title(f'Global {txt_type} Charge Transfer - {label}')
        # ax.legend()
        plt.savefig(f'ctl_corr_global_{ct_type}{label}_all.png', dpi=600)
        plt.close()

        if ct_type == 'nuc':
            muN = np.array([ct.A.mu_minus_global for ct in self.cts])
            muE = np.array([ct.B.mu_plus_global for ct in self.cts])
        elif ct_type == 'ele':
            muN = np.array([ct.B.mu_minus_global for ct in self.cts])
            muE = np.array([ct.A.mu_plus_global for ct in self.cts])

        il = muE < muN
        if il.all():
            return
        if sum(il) < 3:
            return
        fig = plt.figure(figsize=(6, 6))
        ax = plt.gca()
        dNA = self.dNA_global(ct_type)
        x = np.array(dNA).reshape(-1, 1)[il]
        if ct_type == 'nuc':
            x = abs(x)
        y = self.exp_values[il]
        model = LinearRegression()
        model.fit(x, y)
        r_sq = model.score(x, y)
        intercept = model.intercept_
        slope = model.coef_[0]
        y_pred = intercept + slope * x
        ax.plot(x, y_pred, 'k', label=label_line)
        ax.scatter(x, y, 60, label=label_r2)
        label_line = f'$\log k={slope:.4f}{label_dna}{intercept:+.4f}$'
        label_r2 = f'$R^2 = {r_sq:.4f}$'
        labeltxt = f'{label_line}    {label_r2}'
        fig.text(0.5, 0.91, labeltxt, fontsize=12, ha='center', bbox=props)
        ax.tick_params(axis='both', which='both', direction='in', top=True, right=True)
        ax.tick_params(axis='both', which='major', length=10, width=1.5)
        ax.tick_params(axis='both', which='minor', length=5, width=1)
        ax.xaxis.set_label_position('bottom')
        ax.yaxis.set_label_position('left')
        ax.xaxis.set_major_formatter('{x:.2f}')
        ax.minorticks_on()
        ax.set_xlabel(f'${label_dna}$', fontsize=20, fontweight='bold')
        ax.set_ylabel('$\log k$', fontsize=20, fontweight='bold')
        plt.savefig(f'ctl_corr_global_{ct_type}{label}.png', dpi=600)
        plt.close()

    def report_global(self):
        res = f'Charge Transfer Global\n'
        f_name = str(max([len(ct.name) for ct in self.cts] + [8]))
        textMolecule = f'\n{"Molecule":{f_name}} | '
        res += textMolecule + self.cts[0]._str_header_global('A', logk=True)
        for ct, logk in zip(self.cts, self.exp_values):
            res += f'{ct.name:{f_name}} | ' + ct.str_A_global(logk)
        res += textMolecule + self.cts[0]._str_header_global('B')
        res += f'{self.moleculeB:{f_name}} | ' + ct.str_B_global()
        return res

    def report(self, ct_type: str, local_selection: str = 'fukui', name: str = 'charge_transfer_report',
               label: str = ''):
        text = self.report_global() + '\n'
        (a, b), _ = self.get_best_group(ct_type, local_selection)
        text += self.report_local(a, b, ct_type, local_selection)

        # TODO: add correlations with experimental data

        # TODO: refactor
        corr, intercept, slope = self.correlation_global(ct_type)
        alpha = intercept
        beta = slope
        # if ct_type == 'nuc':
        #     etaN = np.array([ct.A.eta_plus_global for ct in self.cts])
        #     etaE = np.array([ct.B.eta_minus_global for ct in self.cts])
        #     if isinstance(self.cts[0].A, OneParabola):
        #         AN = np.array([ct.A.A for ct in self.cts])
        #         IE = np.array([ct.B.I for ct in self.cts])
        #         beta /= 2
        #     else:
        #         AN = np.array([ct.A.mu_minus_global for ct in self.cts])
        #         IE = np.array([ct.B.mu_plus_global for ct in self.cts])
        #
        #     N = (alpha * etaN - beta * AN)
        #     E = (alpha * etaE + beta * IE)
        # elif ct_type == 'ele':
        #     etaN = np.array([ct.B.eta_minus_global for ct in self.cts])
        #     etaE = np.array([ct.A.eta_plus_global for ct in self.cts])
        #     if isinstance(self.cts[0].A, OneParabola):
        #         IN = np.array([ct.A.I for ct in self.cts])
        #         AE = np.array([ct.B.A for ct in self.cts])
        #         beta /= 2
        #     else:
        #         IN = np.array([ct.A.mu_minus_global for ct in self.cts])
        #         AE = np.array([ct.B.mu_plus_global for ct in self.cts])
        #
        #     N = (alpha * etaN + beta * IN)
        #     E = (alpha * etaE - beta * AE)
        # else:
        #     raise ValueError
        if ct_type == 'nuc':
            muN = np.array([ct.A.mu_minus_global for ct in self.cts])
            muE = np.array([ct.B.mu_plus_global for ct in self.cts])
            etaN = np.array([ct.A.eta_minus_global for ct in self.cts])
            etaE = np.array([ct.B.eta_plus_global for ct in self.cts])
        elif ct_type == 'ele':
            muN = np.array([ct.B.mu_minus_global for ct in self.cts])
            muE = np.array([ct.A.mu_plus_global for ct in self.cts])
            etaN = np.array([ct.B.eta_minus_global for ct in self.cts])
            etaE = np.array([ct.A.eta_plus_global for ct in self.cts])

        il = np.ones(len(muN), dtype=bool)
        if any(muE >= muN):
            il = muE < muN
            muN = muN[il]
            muE = muE[il]
            etaN = etaN[il]
            etaE = etaE[il]
            if sum(il) < 3:
                with open(f'{name}{label}.txt', 'w') as f:
                    print(text, file=f)
                breakpoint()
                raise ValueError('µN < µE')

        alpha = 0
        N = (alpha * etaN + beta * muN)
        E = (alpha * etaE - beta * muE)

        SN = 1 / (etaN + etaE)

        # CDFT
        names = [ct.name for c, ct in zip(il, self.cts) if c]
        f_name = str(max([len(name) for name in names] + [8]))
        text += '\nCDFT\n'
        text += f'Correlation\tr^2 = {corr:.4f}\n'
        sign = "-" if intercept < 0 else "+"
        text += f'log k = {slope:8.4f} ΔN {sign} {abs(intercept):8.4f}\n'
        if self.data_mayr is None:
            mayr_NE = ''
            values_text = '\n'.join(f'{name:{f_name}} {s:.4f}  {n:.4f}  {e:.4f}'
                                    for name, s, n, e in zip(names, SN, N, E))
        else:
            mayr_NE = 'N_Mayr' if ct_type == 'nuc' else 'E_Mayr'
            values_text = '\n'.join(f'{name:{f_name}} {s:.4f}  {n:.4f}  {e:.4f}  {exp_ne:>9.4f}'
                                    for name, s, n, e, exp_ne in zip(names, SN, N, E, self.exp_values))

        text += f'{"Molecule":{f_name}}    sN      N         E           {mayr_NE}\n'
        text += f'{values_text}\n'

        if self.data_mayr is not None:
            if ct_type == 'nuc':
                x = self.data_mayr['N'].reshape(-1, 1)[il]
                y = N
                labelNE = 'N'
            else:
                x = self.data_mayr['E'].reshape(-1, 1)[il]
                y = E
                labelNE = 'E'
            y = y.reshape(-1, 1)
            model = LinearRegression()
            model.fit(x, y)
            r_sq = model.score(x, y)
            intercept = model.intercept_[0]
            slope = model.coef_[0, 0]

            text += f'\nCorrelation with Mayr {labelNE} values\t r^2 = {r_sq:.6f}\n'
            sign = "-" if intercept < 0 else "+"
            meq = f'{labelNE}_Mayr = {slope:8.4f} {labelNE}_pred {sign} {abs(intercept):.4f}'
            text += meq + '\n'

            fig, ax = plt.subplots(figsize=(6, 6))
            y_pred = slope * x + intercept
            label_eq = f'$\mathrm{{{labelNE}}}^{{Mayr}}={slope:.2f}\mathrm{{{labelNE}}}^{{CDFT}}{intercept:+.2f}$'
            label_r2 = f'$R^2 = {r_sq:.3f}$'
            labeltxt = f'{label_eq}    {label_r2}'
            ax.plot(x, y_pred, 'k', label=label_eq)
            ax.scatter(x, y, 60, label=label_r2)

            props = dict(facecolor='white', edgecolor='white', boxstyle='round,pad=0.5')
            fig.text(0.5, 0.91, labeltxt, fontsize=12, ha='center', bbox=props)

            ax.tick_params(axis='both', which='both', direction='in', top=True, right=True)
            ax.tick_params(axis='both', which='major', length=10, width=1.5)
            ax.tick_params(axis='both', which='minor', length=5, width=1)
            ax.xaxis.set_label_position('bottom')
            ax.yaxis.set_label_position('left')
            ax.xaxis.set_major_formatter('{x:.2f}')
            ax.minorticks_on()

            ax.set_xlabel(f"$\mathrm{{{labelNE}}}^{{Mayr}}$", fontsize=20, fontweight='bold')
            ax.set_ylabel(f"$\mathrm{{{labelNE}}}^{{CDFT}}$", fontsize=20, fontweight='bold')
            ax.yaxis.set_label_coords(-0.08, 0.5)
            # plt.title("Reactivity parameter correlation")
            # plt.legend()
            plt.savefig(f'Mayr_corr{label}.png', dpi=600)

            if ct_type == 'nuc' and len(self.data_mayr['SN'] > 0):
                x = self.data_mayr['SN'].reshape(-1, 1)
                y = SN.reshape(-1, 1)
                model = LinearRegression()
                model.fit(x, y)
                r_sq = model.score(x, y)
                intercept = model.intercept_[0]
                slope = model.coef_[0, 0]

                plt.figure()
                plt.scatter(x, y)
                y_pred = slope * x + intercept
                label_eq = f'$S_N^{{Mayr}}={slope:.2f}S_N^{{CDFT}}{intercept:+.2f}$'
                plt.plot(x, y_pred, label=label_eq)
                plt.xlabel(f"$S_N^{{Mayr}}$")
                plt.ylabel(f"$S_N^{{CDFT}}$")
                plt.title(f'$r^2 = {r_sq:.4f}$')
                plt.legend()
                plt.savefig(f'Mayr_corrSN{label}.png', dpi=300)

        with open(f'{name}{label}.txt', 'w') as f:
            print(text, file=f)
