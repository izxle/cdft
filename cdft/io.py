from __future__ import annotations

import re
import warnings
from pathlib import Path
from typing import Optional

import numpy as np
import numpy.typing as npt
from ase import Atoms
from ase.io import read
from ase.neighborlist import build_neighbor_list

from cdft.descriptors import DescriptorsModel, OneParabola, TwoParabola, TwoParabolaCh, TwoParabolaChApproximation, \
    GeneralizedQuadratic


def read_gaussian_out(filename: str | Path) -> Atoms:
    atoms = read(filename, format='gaussian-out')

    # Read charge, multiplicity, HOMO and LUMO information from outfile
    with open(filename, 'r') as f:
        text = f.read()
    m = re.search(r'Charge =  ?(?P<charge>-?\d) Multiplicity = (?P<mult>\d).+'
                  r'(?:occ\. eigenvalues --  +(?:-?\d+\.\d+ +)* *(?P<SOMO1>-?\d+\.\d+) +(?P<HOMO1>-?\d+\.\d+)|'
                  r'occ\. eigenvalues --  +(?:-?\d+\.\d+ +)+ *(?P<SOMO2>-?\d+\.\d+)\s+'
                  r'Alpha  occ\. eigenvalues --  +(?P<HOMO2>-?\d+\.\d+))\s+'
                  r'\w+ virt\. eigenvalues -- +(?P<LUMO>-?\d+\.\d+) +(?P<SUMO>-?\d+\.\d+).+'
                  r'(?P<charges_str>(?:Hirshfeld charges, spin densities.+?Tot)|(?:Lowdin Atomic Charges:.+?Sum))',
                  text, re.DOTALL)

    if m is None:
        raise ValueError('Check ME.')

    HOMO = float(m.group('HOMO1') or m.group('HOMO2'))
    SOMO = float(m.group('SOMO1') or m.group('SOMO2'))
    LUMO = float(m.group('LUMO'))
    SUMO = float(m.group('SUMO'))
    threshold = 1e-3
    if HOMO - SOMO < threshold:
        warnings.warn(f'{Path(filename).stem}: HOMO and SOMO are too close.')
    if LUMO - SUMO < threshold:
        warnings.warn(f'{Path(filename).stem}: LUMO and SUMO are too close.')
    atoms.info['charge'] = int(m.group('charge'))
    atoms.info['mult'] = int(m.group('mult'))
    atoms.info['HOMO'] = HOMO
    atoms.info['SOMO'] = SOMO
    atoms.info['LUMO'] = LUMO
    atoms.info['SUMO'] = SUMO

    charges_str = re.findall(r' +\d+ +[A-Z][a-z]? +(-?\d+\.\d+)', m.group('charges_str'))
    charges = np.array([float(c) for c in charges_str])
    atoms.set_array('charges', charges)
    return atoms


def read_from_CT_directory(directory: str | Path, model: str = 'one parabola', format: str = 'xyz',
                           suffix: str = '.xyz') -> DescriptorsModel:
    # Read files and get AtomsCT object
    if format == 'xyz':
        atoms = AtomsCT.from_CT_directory(directory)
    elif format == 'gaussian-out':
        atoms = AtomsCT.from_CT_directory_gaussian(directory, suffix)
    # Build a ChargeTransfer object for the appropriate model
    if model in ['one parabola', '1P', 'PP']:
        ct = OneParabola(atoms)
    elif model in ['two parabola', '2P']:
        ct = TwoParabola(atoms)
    elif model in ['two parabola chamorro', '2PCh']:
        ct = TwoParabolaCh(atoms)
    elif model in ['two parabola chamorro approximation', '2PChA']:
        ct = TwoParabolaChApproximation(atoms)
    elif model in ['generalized quadratic', 'GQ']:
        if 'HOMO' not in atoms.info or 'LUMO' not in atoms.info:
            raise ValueError('The Generalized Quadratic Model needs HOMO and LUMO energies.')
        ct = GeneralizedQuadratic(atoms)
    else:
        raise ValueError(f'Unexpected model: {model}')
    return ct


class AtomsCT(Atoms):
    def __init__(self, name: str, I: float, A: float, fukui_plus: npt.ArrayLike, fukui_minus: npt.ArrayLike,
                 basecharge: float = 0, HOMO: Optional[float] = None, LUMO: Optional[float] = None, **kwargs):
        super().__init__(**kwargs)
        self.name = name
        self.I = I
        self.A = A
        self.fukui_plus = np.array(fukui_plus)
        self.fukui_minus = np.array(fukui_minus)
        self.charge = basecharge
        self.HOMO = HOMO
        self.LUMO = LUMO
        self.neighbor_list = build_neighbor_list(self, bothways=True, self_interaction=False)

    def get_neighbors(self, i: int):
        return self.neighbor_list.get_neighbors(i)[0]

    @classmethod
    def from_CT_directory_gaussian(cls, directory: str | Path, suffix: str = '.out') -> AtomsCT:
        """
        Construct AtomsCT object from the directory of Gaussian calculations
        Expects to find three files ending in _{charge}{suffix} where charge will be [0, 1, -1]
        :param directory: Path to where the gaussian-out files with charges are located
        :param suffix: Suffix of the gaussian-out files
        :return: AtomsCT
        """
        folder = Path(directory)
        name = folder.name
        neutral = folder / f'{name}_0{suffix}'
        positive = folder / f'{name}_1{suffix}'
        negative = folder / f'{name}_-1{suffix}'
        atoms_ct = cls.from_gaussian(neutral, positive, negative, name)
        return atoms_ct

    @classmethod
    def from_CT_directory(cls, directory: str | Path) -> AtomsCT:
        """
        Construct AtomsCT object from the directory of a ΔSCF calculation created by the cdft library
        :param directory: Path to where the xyz files with charges are located
        :return: AtomsCT
        """
        folder = Path(directory)
        name = folder.name
        neutral = folder / f'{name}_0.xyz'
        positive = folder / f'{name}_1.xyz'
        negative = folder / f'{name}_-1.xyz'
        atoms_ct = cls.from_xyz(neutral, positive, negative, name)
        return atoms_ct

    @classmethod
    def from_xyz(cls, neutral: str | Path, positive: str | Path, negative: str | Path,
                 name: Optional[str] = None) -> AtomsCT:
        """
        Construct AtomsCT object from three xyz files containing the charges for a system with charge {0, +1, -1}
        :param neutral: xyz file for neutral molecule
        :param positive: xyz file for positive molecule
        :param negative: xyz file for negative molecule
        :param name: name for the system to use in comparisons
        :return: AtomsCT
        """
        if name is None:
            name = Path(neutral).stem
        # read files
        atoms_neutral = read(neutral)
        atoms_positive = read(positive)
        atoms_negative = read(negative)
        atoms_ct = cls.from_atoms(atoms_neutral, atoms_positive, atoms_negative, name)
        return atoms_ct

    @classmethod
    def from_gaussian(cls, neutral: str | Path, positive: str | Path, negative: str | Path, name: Optional[str] = None):
        atoms_neutral = read_gaussian_out(neutral)
        atoms_positive = read_gaussian_out(positive)
        atoms_negative = read_gaussian_out(negative)
        atoms_ct = cls.from_atoms(atoms_neutral, atoms_positive, atoms_negative, name)
        return atoms_ct

    @classmethod
    def from_atoms(cls, neutral: Atoms, positive: Atoms, negative: Atoms, name: str = 'charge_transfer') -> AtomsCT:
        """
        Construct AtomsCT object from three ase.Atoms objects containing charges for a system with charge {0, +1, -1}
        :param neutral: Atoms object of system with charge 0
        :param positive: Atoms object of system with charge +1
        :param negative: Atoms object of system without charge -1
        :param name: string identifier for this system
        :return: AtomsCT
        """
        # TODO: check that all three structures have the same coordinates
        # check that all three files have charge information
        # assert neutral.get_initial_charges().any(), 'Missing charge information of neutral system'
        # assert positive.get_initial_charges().any(), 'Missing charge information of positive system'
        # assert negative.get_initial_charges().any(), 'Missing charge information of negative system'

        # Ionization potential and electron affinity
        basecharge = neutral.info['charge']
        I = positive.get_potential_energy() - neutral.get_potential_energy()
        A = neutral.get_potential_energy() - negative.get_potential_energy()

        # calculate fukui functions
        if 'charges' in neutral.calc.results:
            fukui_plus = neutral.get_charges() - negative.get_charges()
            fukui_minus = positive.get_charges() - neutral.get_charges()
        elif 'initial_charges' in neutral.arrays:
            fukui_plus = neutral.get_initial_charges() - negative.get_initial_charges()
            fukui_minus = positive.get_initial_charges() - neutral.get_initial_charges()
        else:
            raise ValueError(f'Charges not found on Atoms for {name}')

        HOMO = neutral.info.get('HOMO')
        LUMO = neutral.info.get('LUMO')

        return cls(name, I, A, fukui_plus, fukui_minus, basecharge=basecharge, HOMO=HOMO, LUMO=LUMO, symbols=neutral)
