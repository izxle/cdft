from __future__ import annotations

from typing import Dict

from ase.calculators.calculator import FileIOCalculator
from ase.calculators.gaussian import Gaussian

from cdft.tasks.config import error


def build_calc(name: str, calc_name: str, calc_params: Dict[str, str], **extra_params) -> FileIOCalculator:
    params = calc_params.copy()
    if 'steps' in params:
        del params['steps']
    if 'optimization' in params:
        del params['optimization']
    if 'label' not in params:
        params['label'] = name
    params.update(extra_params)
    if calc_name == 'gpaw':
        from gpaw import GPAW
        if 'txt' not in params:
            params['txt'] = name + '.out'
        calculator = GPAW(**params)
    elif calc_name == 'gaussian':
        params['chk'] = name + '.chk'
        if name.endswith('_opt'):
            params['freq'] = None
        calculator = Gaussian(**params)
    elif calc_name == 'aimnetnse':
        try:
            from aimnetnse import AIMNetNSECalculator, load_models
        except ImportError:
            error('AIMNetNSE calculator not found.')
        import torch
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        model = load_models(device)
        # TODO: check if we can pass calc_params to AIMNetNSE
        charge = params.get('charge', 0)
        mult = params['mult']
        calculator = AIMNetNSECalculator(model, charge=charge, mult=mult)
    elif calc_name == 'aimnet':
        try:
            from aimnetnse import AIMNet2Calculator, load_models
        except ImportError:
            error('AIMNet2 calculator not found.')
        model = load_models(version='2')
        calculator = AIMNet2Calculator(model)
        charge = params.get('charge', 0)
        calculator.set_charge(charge)
    else:
        raise NotImplementedError(f'Calculator {calc_name} not yet implemented.')
    return calculator
