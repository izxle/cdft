import re
from typing import Tuple, List, Dict

import numpy as np
from ase import Atoms
from ase.io import read


def read_dSCF_fukui_gaussian(name: str, suffix: str = '.log') -> Tuple[List[str], float, float, np.ndarray, np.ndarray]:
    partial_charges: Dict[int, np.ndarray] = dict()

    re_hirshfeld = re.compile(r'Hirshfeld charges, spin densities.+?Tot', re.DOTALL)
    re_lowdin = re.compile(r'Lowdin Atomic Charges:.+?Sum', re.DOTALL)
    re_charges = re.compile(r' +\d+ +[A-Z][a-z]? +(-?\d+\.\d+)')

    atoms_dict: Dict[int, Atoms] = dict()
    for charge in {0, -1, 1}:
        # read calculation results
        filename = f'{name}_{charge}{suffix}'
        atoms = read(filename, format='gaussian-out')
        with open(filename, 'r') as f:
            text = f.read()

        # read block of text with partial charges
        text_block = re_hirshfeld.search(text)
        if text_block is None:
            text_block = re_lowdin.search(text)
        if text_block is None:
            raise ValueError(f"Couldn't recognize charges in logfile of {filename}")

        charges_str = re_charges.findall(text_block.group())
        tmp_charges = [float(c) for c in charges_str]

        partial_charges[charge] = np.array(tmp_charges)
        atoms_dict[charge] = atoms

    # assume partial charge information is in the same atom order as in atoms
    labels = atoms.get_chemical_symbols()

    energy_zero = atoms_dict[0].get_potential_energy()
    energy_plus = atoms_dict[1].get_potential_energy()
    energy_minus = atoms_dict[-1].get_potential_energy()
    I = energy_plus - energy_zero
    A = energy_zero - energy_minus

    # TODO: check if nucleophile/electrophile affects calculation
    fukui_plus = partial_charges[0] - partial_charges[-1]
    fukui_minus = partial_charges[1] - partial_charges[0]

    return labels, I, A, fukui_plus, fukui_minus


def read_atoms_charges_gaussian(filename):
    re_hirshfeld = re.compile(r'Hirshfeld charges, spin densities.+?Tot', re.DOTALL)
    re_lowdin = re.compile(r'Lowdin Atomic Charges:.+?Sum', re.DOTALL)
    re_charges = re.compile(r' +\d+ +[A-Z][a-z]? +(-?\d+\.\d+)')

    # read atoms object from outfile
    atoms = read(filename, format='gaussian-out')

    # read whole outfile
    with open(filename, 'r') as f:
        text = f.read()

    # read block of text with partial charges
    text_block = re_hirshfeld.search(text)
    if text_block is None:
        text_block = re_lowdin.search(text)
    if text_block is None:
        raise ValueError(f"Couldn't recognize charges in logfile of {filename}")

    charges_str = re_charges.findall(text_block.group())
    charges = np.array([float(c) for c in charges_str])

    atoms.calc.results['charges'] = charges

    return atoms
