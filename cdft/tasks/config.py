from __future__ import annotations

import sys
from configparser import ConfigParser
from dataclasses import dataclass, field
from pathlib import Path
from typing import List, Dict, Optional, Any, Tuple
from abc import ABC

supported_calculators = {'gpaw', 'gaussian', 'demon', 'aimnetnse', 'aimnet'}


@dataclass
class AnalysisConfiguration(ABC):
    ...


@dataclass
class ChargeTransferAnalysisConfiguration(AnalysisConfiguration):
    name: str
    directory: Path
    model: str
    local_selection: str
    type: str
    reagent: str
    data: Optional[Dict[str, float]] = None
    data_mayrN: Optional[Dict[str, float]] = None
    data_mayrE: Optional[Dict[str, float]] = None
    data_mayrSN: Optional[Dict[str, float]] = None
    group: Optional[Tuple[int, int]] = None


@dataclass
class ElectrophilicityAnalysisConfiguration(AnalysisConfiguration):
    name: str
    directory: Path
    model: str
    data_mayr: Path = None
    exclude: List[str] = field(default_factory=list)
    ignore: List[str] = field(default_factory=list)
    groups: List[str] = field(default_factory=list)


@dataclass
class FLPAnalysisConfiguration(AnalysisConfiguration):
    name: str
    models: List[str]
    reference: str
    local_atom: int
    distance_atom: int


@dataclass
class RunConfiguration:
    task: str
    calc_name: str
    calc_out_suffix: str
    calc_in_suffix: str
    calc_params: Dict[str, str]
    directory_in: Path
    directory_out: Path
    filenames: List[Path]
    run_system: str
    run_params: Dict[str, str]
    overwrite: bool
    vacuum: Optional[float] = None
    parallel: bool = False
    analysis: Optional[AnalysisConfiguration] = None
    analysis_params: Dict[str, Any] = field(default_factory=dict)
    test: bool = False
    verbose: bool = False


def error(msg):
    print(msg, file=sys.stderr)
    sys.exit(1)


def parse_config(filename: str | Path) -> RunConfiguration:
    if not Path(filename).is_file():
        error(f"FileNotFoundError: file '{filename}' does not exist.")
    # initialize config parser
    config = ConfigParser()
    # read config file
    config.read(filename)

    # SECTION General
    if not config.has_section('General'):
        error(f"Config file {filename} doesn't have required 'General' section")
    # get task name
    if not config.has_option('General', 'task'):
        error("Expected section 'General' to have option 'task'")
    task = config['General']['task'].strip().lower()
    # TODO: autoupdate list of available tasks
    available_tasks = ['charge transfer', 'BOPA', 'electrophilicity', 'calc', 'flp', 'gamma', 'report', 'read', 'perturbed']
    if not any(t in task for t in available_tasks):
        error(f'Task `{task}` not in available tasks: {available_tasks}')
    # get filenames of structures to run
    directory_in = Path(config.get('General', 'structures', fallback='.'))
    suffix = config.get('General', 'suffix', fallback='.xyz')
    filenames = list(directory_in.rglob(f'*{suffix}'))
    # get calculator name
    if not config.has_option('General', 'Calculator'):
        error("Expected section 'General' to have option 'Calculator'")
    calc_name = config['General']['Calculator'].lower()
    if calc_name not in supported_calculators:
        error(f'calculator {calc_name} not supported')
    calc_out_suffix = config.get('General', 'out', fallback='.out')
    calc_in_suffix = config.get('General', 'in', fallback='.in')
    directory_out = Path(config.get('General', 'results_directory', fallback='calculations'))
    # get vacuum information
    vacuum = config.getfloat('General', 'vacuum', fallback=None)
    # read parallel option for gpaw calculations
    parallel = config.getboolean('General', 'parallel', fallback=False) if calc_name == 'gpaw' else False

    # SECTION calculator
    if not config.has_section('Calculator'):
        error(f"Config file {filename} doesn't have required 'Calculator' section")
    # get parameters to pass to calculator builder
    calc_params = dict(config['Calculator'])
    # maximum number of optimization steps (for ASE optimizations)
    calc_params['steps'] = config.getint('Calculator', 'steps', fallback=1000)
    if calc_name in ['ase', 'aimnet', 'aimnetnse', 'aimnet', 'gaussian']:
        calc_params['optimization'] = config.getboolean('Calculator', 'optimization', fallback=True)

    # SECTION Run
    if not config.has_section('Run'):
        error(f"Config file {filename} doesn't have required 'Run' section")
    # check for valid values
    if not config.has_option('Run', 'system'):
        error("Option 'system' for section 'Run' missing.")
    runner = config.get('Run', 'system').lower()
    if runner != 'ase' and runner not in {'slurm', 'pbs', 'test', 'lsf'}:
        error(f'{runner} not supported')
    # get overwrite preference
    overwrite = config.getboolean('Run', 'overwrite', fallback=False)
    # testing option
    test = config.getboolean('Run', 'test', fallback=False)

    # SECTION Queue
    if runner != 'ase':
        if not config.has_option('Queue', 'queue'):
            error("Expected section 'Run' to have option 'queue'.")
        run_info = dict(config['Queue'])
    else:
        run_info = dict()

    # SECTION Analysis
    if config.has_section('Charge Transfer'):
        # output directory
        directory_analysis = Path(config.get('Charge Transfer', 'directory', fallback='charge_transfer'))
        # charge transfer model ['one parabola', 'two parabola']
        ct_model = config.get('Charge Transfer', 'model', fallback='one parabola').split(',')
        # get type of analysis ['nuc', 'ele']
        if not config.has_option('Charge Transfer', 'type'):
            error("Expected section 'Charge Transfer' to have option 'type'.")
        nucele = config.get('Charge Transfer', 'type').lower()
        if nucele not in {'nuc', 'ele'}:
            error(f"Invalid value `{nucele}` for option 'type' of section 'Charge Transfer'. Use 'nuc' or 'ele'.")
        # get selection method for the local charge transfer model
        ct_local_selection_method = config.get('Charge Transfer', 'selection', fallback='fukui')
        # get name of reagent
        if not config.has_option('Charge Transfer', 'reagent'):
            error("Expected section 'Charge Transfer' to have option 'reagent'.")
        reagent = config.get('Charge Transfer', 'reagent')
        # get experimental data for correlations
        analysis_data: Dict[str, float] = dict()
        data_str = config.get('Charge Transfer', 'data', fallback=None)
        if data_str:
            for line in data_str.split('\n'):
                key, value = line.split('=')
                analysis_data[key.strip()] = float(value)
        # get Nuclephilicity or Electrophilicity values (depending on type of analysis)
        data_mayrN: Dict[str, float] = dict()
        data_mayrN_str = config.get('Charge Transfer', 'MayrN', fallback=None)
        if data_mayrN_str:
            for line in data_mayrN_str.split('\n'):
                key, value = line.split('=')
                data_mayrN[key.strip()] = float(value)

        data_mayrSN: Dict[str, float] = dict()
        data_mayrSN_str = config.get('Charge Transfer', 'MayrSN', fallback=None)
        if data_mayrSN_str:
            for line in data_mayrSN_str.split('\n'):
                key, value = line.split('=')
                data_mayrSN[key.strip()] = float(value)

        data_mayrE: Dict[str, float] = dict()
        data_mayrE_str = config.get('Charge Transfer', 'MayrE', fallback=None)
        if data_mayrE_str:
            for line in data_mayrE_str.split('\n'):
                key, value = line.split('=')
                data_mayrE[key.strip()] = float(value)

        # specify group to analyze, for the local charge transfer model
        group = config.get('Charge Transfer', 'group', fallback='').split()
        if group:
            group = (int(group[0]), int(group[1]))
        else:
            group = None
        # build AnalysisConfiguration Object
        analysis = ChargeTransferAnalysisConfiguration(name=task,
                                                       directory=directory_analysis,
                                                       model=ct_model,
                                                       type=nucele,
                                                       local_selection=ct_local_selection_method,
                                                       reagent=reagent,
                                                       data=analysis_data,
                                                       data_mayrN=data_mayrN,
                                                       data_mayrSN=data_mayrSN,
                                                       data_mayrE=data_mayrE,
                                                       group=group)
    elif config.has_section('Electrophilicity'):
        directory_analysis = Path(config.get('Electrophilicity', 'directory', fallback='calculations'))
        # charge transfer model ['one parabola', 'two parabola']
        ct_model = config.get('Electrophilicity', 'model', fallback='two parabola')
        # read group classifications to ignore in the analysis
        exclude = config.get('Electrophilicity', 'exclude', fallback='').split()
        # molecules to ignore in the analysis
        ignore = config.get('Electrophilicity', 'ignore', fallback='').split()

        # read group classifications to take into account
        # if this field is given, only these values will be used to plot
        groups = config.get('Electrophilicity', 'groups', fallback='').split()

        # charge transfer model ['one parabola', 'two parabola']
        filename_mayr = config.get('Charge Transfer', 'data', fallback=None)
        if filename_mayr is None:
            filename_mayr = Path(__file__).parent.parent.parent / 'data/MayrDatabase.csv'

        analysis = ElectrophilicityAnalysisConfiguration(name=task,
                                                         directory=directory_analysis,
                                                         model=ct_model,
                                                         data_mayr=filename_mayr,
                                                         exclude=exclude,
                                                         ignore=ignore,
                                                         groups=groups)
    elif config.has_section('FLP ANALYSIS'):
        models = config.get('FLP ANALYSIS', 'models').split()
        # TODO: add reference
        reference = config.get('FLP ANALYSIS', 'reference', fallback=None)
        local_atom = config.get('FLP ANALYSIS', 'local_atom', fallback=None)
        distance_atom = config.get('FLP ANALYSIS', 'distance_atom', fallback=None)

        analysis = FLPAnalysisConfiguration(name=task,
                                            models=models,
                                            reference=reference,
                                            local_atom=local_atom,
                                            distance_atom=distance_atom)
    else:
        analysis = None

    # build RunConfiguration object with all read values
    res = RunConfiguration(task=task,
                           calc_name=calc_name,
                           calc_out_suffix=calc_out_suffix,
                           calc_in_suffix=calc_in_suffix,
                           calc_params=calc_params,
                           filenames=filenames,
                           directory_in=directory_in,
                           directory_out=directory_out,
                           run_system=runner,
                           run_params=run_info,
                           analysis=analysis,
                           vacuum=vacuum,
                           parallel=parallel,
                           overwrite=overwrite,
                           test=test)
    return res

