
script = '''#!/bin/bash
#BSUB -q $QUEUE
#BSUB -J $JOBNAME
#BSUB -n $NPROC
#BSUB -e $ERR
#BSUB -o $OUT
#BSUB -R span[hosts=1]

$EXTRA_OPTIONS

echo "$LSB_JOBNAME $LSB_JOBID user: $USER"
echo "Time at submission: $(date)"

# load necessary modules
module purge
$MODULES

hostname -s | sort > ${LSB_JOBNAME}_hostlist.dat

init=$(date +%s)
$COMMAND
fin=$(date +%s)

echo "Total execution time: $(( fin - init )) s"
'''
# TODO: add the -R option to the configuration options

# name, label, default
values = (
    ('nproc', '$NPROC', '1'),
    ('time', '$TIME', '01-00:00:00'),
    ('error', '$ERR', '$JOBNAME_%j.err'),
    ('out', '$OUT', '$JOBNAME_%j.out'),
    ('modules', '$MODULES', '# no additional modules loaded'),
    ('command', '$COMMAND', 'echo test'),
)


def build_LSF_script(data: dict, **kwargs):
    data = data.copy()
    data.update(**kwargs)
    res = script.replace('$QUEUE', data['queue'])
    jobname = data.get('jobname', 'test')
    for name, label, default in values:
        res = res.replace(label, data.get(name, default))
    res = res.replace('$JOBNAME', jobname)
    names = [v[0] for v in values]
    extra_keys = [k for k in data if k not in names + ['queue', 'jobname']]
    extra_options = ''
    for name in extra_keys:
        extra_options += f'#BSUB -{name} {data[name]}\n'
    res = res.replace('$EXTRA_OPTIONS', extra_options)
    return res
