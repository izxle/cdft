
script = '''#!/bin/bash
#PBS -q $QUEUE
#PBS -N $JOBNAME
#PBS -l nodes=$NODES:ppn=$PPN,walltime=$TIME
#PBS -e $ERR
#PBS -o $OUT
$DEPENDENCY

$EXTRA_TEXT

echo "$PBS_JOB_NAME $PBE_JOBID user: $USER"
echo "Time at submission: $(date)"

 
cd $PBS_O_WORKDIR

# load necessary modules
$MODULES

mkdir -p $PBS_O_WORKDIR/admon;
env > $PBS_O_WORKDIR/admon/environment-$PBS_JOBID.txt
cat $PBS_NODEFILE | sort | uniq > $PBS_O_WORKDIR/admon/node-list-$PBS_JOBID.txt

init=$(date +%s)
$COMMAND
fin=$(date +%s)


echo "Total execution time: $(( fin - init )) s"
'''

# name, label, default
values = (
    ('jobname', '$JOBNAME', 'test'),
    ('nodes', '$NODES', '1'),
    ('ppn', '$PPN', '1'),
    ('error', '$ERR', '${PBS_JOBNAME}_$PBS_JOBID.err'),
    ('out', '$OUT', '${PBS_JOBNAME}_$PBS_JOBID.out'),
    ('extra', '$EXTRA_TEXT', ''),
    ('modules', '$MODULES', '# no additional modules loaded'),
    ('command', '$COMMAND', 'echo test'),
)


def build_PBS_script(data: dict, **kwargs):
    data = data.copy()
    data.update(**kwargs)
    res = script.replace('$QUEUE', data['queue'])
    if 'time' in data:
        res = res.replace('$TIME', data['data'])
    else:
        res = res.replace(',walltime=$TIME', '')

    if 'dependency' in data:
        dependency_text = '#PBS -W depend=' + data['dependency']
    else:
        dependency_text = ''
    res = res.replace('$DEPENDENCY', dependency_text)

    for name, label, default in values:
        res = res.replace(label, data.get(name, default))

    return res
