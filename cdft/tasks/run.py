from __future__ import annotations

import re
import subprocess
import sys
import warnings
from pathlib import Path
from typing import Tuple, Optional

import numpy as np
from ase import Atoms
from ase.calculators.calculator import PropertyNotImplementedError
from ase.calculators.gaussian import GaussianOptimizer
from ase.io import read
from ase.optimize import BFGS
from ase.parallel import paropen
from ase.utils import workdir
from ase.optimize.sciopt import OptimizerConvergenceError
from tqdm import tqdm

from cdft.io import read_gaussian_out
from cdft.tasks.calculator.calculator import build_calc
from cdft.tasks.config import RunConfiguration, parse_config
from cdft.tasks.lsf import build_LSF_script
from cdft.tasks.pbs import build_PBS_script
from cdft.tasks.slurm import build_SLURM_script
from cdft.tasks.tasks import charge_transfer_analysis, electrophilicity_analysis, flp_analysis, \
    electrophilicity_gamma_optimization, report, outputs2extxyz, perturbed_parameter_optimization, \
    plot_perturbed_gammas, test_gamma_mayr


def BOPA(atoms: Atoms, calculator):
    atoms.calc = calculator
    atoms.get_potential_energy()


def relax(atoms: Atoms, basename: str, config: RunConfiguration) -> Tuple[Atoms, Optional[str]]:
    name = f'{basename}_opt'
    # return relaxed system if it already exists
    xyz = Path(f'{name}.xyz')
    if xyz.is_file() and not config.overwrite:
        return read(xyz), None

    # add calculator to atoms object
    extra_params = {}
    if config.parallel:
        extra_params['txt'] = paropen(f'{name}{config.calc_out_suffix}', 'w')
    if config.calc_name in ['gaussian', 'aimnetnse']:
        charge = atoms.info.get('charge', 0)
        mult = atoms.info.get('mult', (atoms.get_atomic_numbers().sum() - charge) % 2 + 1)
        config.calc_params.update({'charge': charge,
                                   'mult': mult})

    calculator = build_calc(name, config.calc_name, config.calc_params)
    atoms.calc = calculator

    # define optimization object
    if config.calc_name == 'gaussian':
        opt = GaussianOptimizer(atoms, calculator)
    elif config.calc_name in {'gpaw', 'aimnetnse', 'aimnet'}:
        opt = BFGS(atoms, logfile=name + '.log')
    else:
        raise NotImplementedError(f'Optimization with {config.calc_name} calculator not yet implemented.')

    if config.run_system == 'ase':
        jobid = None
        # run calculation with ase
        steps = config.calc_params.get('steps', 1000)
        try:
            converged = opt.run(steps=steps)
            if not converged:
                raise OptimizerConvergenceError
        except PropertyNotImplementedError as e:
            # this error happens with gaussian
            print(f'{e}\n\nizxle: unknown optimization error\n', file=sys.stderr)
        atoms.write(xyz)
        if config.calc_name in {'gpaw'}:
            atoms.calc.write(f'{name}.gpw')
    else:
        # submit job to queuing system
        # gaussian calculator
        # TODO: change for other calculators
        kw = {}
        opt.set_keywords(kw)
        opt.calc.set(**kw)
        opt.calc.write_input(atoms)

        if config.calc_name == 'gaussian':
            command = config.run_params['command']
            config.run_params['command'] = (f'{command}\n'
                                            f'ase convert {name}{config.calc_out_suffix} '
                                            f'{name}.xyz -i gaussian-out -n -1')

        jobid = submit_to_queue(name, config)
        if config.calc_name == 'gaussian':
            config.run_params['command'] = command

    return atoms, jobid


def calc_is_done(name: str, config: RunConfiguration) -> bool:
    if config.calc_name == 'gaussian':
        outfile = Path(f'{name}{config.calc_out_suffix}')
        if not outfile.is_file():
            return False
        with outfile.open('r') as f:
            lastline = f.readlines()[-1]
        return 'Normal termination' in lastline
    elif config.calc_name == 'gpaw':
        suffix = '.gpw'
    elif config.calc_name in {'aimnetnse', 'aimnet'}:
        suffix = '.xyz'
    else:
        raise NotImplementedError(f'Calculator `{config.calc_name}` not implemented')
    file_out = Path(name + suffix)
    return file_out.is_file()


def dSCF(atoms: Atoms, basename: str, config: RunConfiguration):
    for d_charge in {0, -1, 1}:
        name = f'{basename}_{d_charge}'
        if calc_is_done(name, config) and not config.overwrite:
            continue

        charge = atoms.info.get('charge', 0) + d_charge
        atoms_charge = atoms.copy()
        atoms_charge.info['charge'] = charge
        # remove initial charges and forces because of conflicts
        # TODO: look for more elegant way to remove arrays
        if 'initial_charges' in atoms_charge.arrays:
            del atoms_charge.arrays['initial_charges']
        if 'forces' in atoms_charge.arrays:
            del atoms_charge.arrays['forces']

        # update calculator
        calc_kw = dict(name=name,
                       calc_name=config.calc_name,
                       calc_params=config.calc_params,
                       charge=charge)
        if config.parallel:
            calc_kw['txt'] = paropen(f'{name}{config.calc_out_suffix}', 'w')
        if config.calc_name == 'gaussian':
            multiplicity = (atoms_charge.get_atomic_numbers().sum() - charge) % 2 + 1
            calc_kw['mult'] = multiplicity
            atoms_charge.info['mult'] = multiplicity
        elif config.calc_name == 'aimnetnse':
            multiplicity = (atoms_charge.get_atomic_numbers().sum() - charge) % 2 + 1
            calc_kw['mult'] = multiplicity
            atoms_charge.info['mult'] = multiplicity
        elif config.calc_name == 'aimnet':
            if 'mult' in atoms_charge.info:
                del atoms_charge.info['mult']
        calculator = build_calc(**calc_kw)
        atoms_charge.calc = calculator
        # run calculation
        if config.run_system == 'ase':
            atoms_charge.get_potential_energy()
            if config.calc_name == 'gpaw':
                atoms_charge.calc.write(name + '.gpw')
                # hirshfeld population analysis
                from gpaw.analyse.hirshfeld import HirshfeldPartitioning
                hf = HirshfeldPartitioning(atoms_charge.calc)
                charges = np.array(hf.get_charges())
                atoms_charge.set_initial_charges(charges)
            elif config.calc_name == 'gaussian':
                # Check for Normal termination
                if not calc_is_done(name, config):
                    warnings.warn(f'Abnormal termination for: {name}')
                    continue
                # read HOMO and LUMO from outfile
                atoms_charge = read_gaussian_out(f'{name}{config.calc_out_suffix}')
                if 'charges' in atoms_charge.arrays and 'charges' in atoms_charge.calc.results:
                    del atoms_charge.calc.results['charges']
            filename = f'{name}.xyz'
            atoms_charge.write(filename)
        else:
            if config.calc_name == 'gaussian':
                command = config.run_params['command']
                new_command = (f'# command to get optimized xyz into gaussian input file\n'
                               f'gaussian_parser.py update-input {name}{config.calc_in_suffix} '
                               f'{basename}_opt{config.calc_out_suffix}\n'
                               f'{command}\n'
                               f'gaussian_parser.py out2xyz {name}{config.calc_out_suffix}')

                config.run_params['command'] = new_command
            # submit to queuing system
            calculator.write_input(atoms_charge)
            submit_to_queue(name, config)

            if config.calc_name == 'gaussian':
                config.run_params['command'] = command


def submit_to_queue(name: str, config: RunConfiguration) -> Optional[str]:
    jobid = None
    if config.run_system == 'slurm':
        script_name = Path(f'{name}.slrm')
        script_text = build_SLURM_script(config.run_params, jobname=name)
        command = f'sbatch {script_name}'
    elif config.run_system == 'lsf':
        script_name = Path(f'{name}.lsf')
        script_text = build_LSF_script(config.run_params, jobname=name)
        command = f'bsub < {script_name}'
    elif config.run_system == 'pbs':
        script_name = Path(f'{name}.pbs')
        script_text = build_PBS_script(config.run_params, jobname=name)
        command = f'qsub {script_name}'
    elif config.run_system == 'test':
        script_name = Path(f'{name}.test')
        script_text = build_PBS_script(config.run_params, jobname=name)
        script_text += build_SLURM_script(config.run_params, jobname=name)
        command = f'cat {script_name}'
    else:
        raise NotImplementedError(f'Queuing system {config.run_system} not yet supported.')

    # write submission script
    with script_name.open('w') as f:
        f.write(script_text)

    # run command
    if config.test:
        jobid = '1'
    else:
        pass
    try:
        res = subprocess.run(command, shell=True, check=True, capture_output=True)
    except subprocess.CalledProcessError as e:
        with open(f'{name}.err', 'w') as f:
            print(f'Error when running `{command}`:\n{e.stderr}', file=f)
    else:
        if config.run_system == 'lsf':
            jobid = res.stdout.split()[1].decode().strip('<>')
        else:
            jobid = res.stdout.split()[-1].decode()

    if config.verbose:
        print(f'Submitted job {name}:{jobid}')
    return jobid


def calc_charge_transfer(atoms: Atoms, name: str, config: RunConfiguration):
    basename = Path(name).name
    # create directory file tree
    directory = Path(config.directory_out) / name
    # perform necessary calculations
    with workdir(directory, mkdir=True):
        file_xyz = Path(basename + '.xyz')
        if not file_xyz.is_file() or config.overwrite:
            # write input structure
            atoms.write(file_xyz)

        # run relaxation
        if config.calc_params.get('optimization'):
            atoms_opt, jobid = relax(atoms, basename, config)
        else:
            atoms_opt = atoms
            jobid = None

        # update run_info for queuing system
        if config.run_system != 'ase':
            # add the relaxation job as a dependency to wait before running dSCF
            if jobid:
                if config.run_system in ['slurm', 'pbs']:
                    config.run_params['dependency'] = f'afterok:{jobid}'
                elif config.run_system == 'lsf':
                    config.run_params['w'] = f'"done({jobid})"'

        # run dSCF calculation
        dSCF(atoms_opt, basename, config)

        # clean dependency
        if 'dependency' in config.run_params:
            del config.run_params['dependency']
        if 'w' in config.run_params:
            del config.run_params['w']


def run_task(configfile: str | Path):
    config = parse_config(configfile)
    if 'read' in config.task:
        outputs2extxyz(config)
    elif 'calc' in config.task:
        for filename in tqdm(config.filenames, desc=f'Running {config.task} calculation'):
            filepath = filename.relative_to(config.directory_in)
            # get file basename
            name = filepath.stem
            # remove unwanted characters
            name = re.sub(r'[,()]', '', name)
            # get subdirectory
            subdir = filepath.parent / name
            # read initial structure
            atoms = read(filename)
            # add vacuum if needed
            if config.vacuum:
                atoms.center(vacuum=config.vacuum)
            # select task

            if 'BOPA' in config.task:
                calculator = build_calc(config.calc_name, name, config.calc_params)
                BOPA(atoms, calculator)
            else:
                try:
                    calc_charge_transfer(atoms=atoms, name=subdir, config=config)
                except OptimizerConvergenceError:
                    print(f"Optimization didn't converge: {filepath}")
                except RuntimeError as E:
                    if 'aimnet' in config.calc_name:
                        print(f'skipping {filepath} error in Torch')
                    else:
                        raise E

    # charge transfer analysis
    if config.analysis:
        try:
            if 'report' in config.task:
                report(config)
            elif 'charge transfer' in config.task:
                charge_transfer_analysis(config)
            elif 'electrophilicity' in config.task:
                electrophilicity_analysis(config)
            elif 'perturbed' in config.task:
                # perturbed_parameter_optimization(config)
                plot_perturbed_gammas(config)
            elif 'gamma' in config.task:
                # electrophilicity_gamma_optimization(config)
                test_gamma_mayr(config)
            elif 'flp' in config.task:
                flp_analysis(config)
        except FileNotFoundError as E:
            print('Calculations have not finished yet.', E)
