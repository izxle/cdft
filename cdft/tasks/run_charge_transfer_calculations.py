import sys

from typing import Dict
from pathlib import Path
from argparse import ArgumentParser
from configparser import ConfigParser

from ase.io import read
from ase.utils import workdir
from ase.calculators.gaussian import Gaussian, GaussianOptimizer
from ase.calculators.calculator import PropertyNotImplementedError


def update_gaussian_kw(calculator: Dict, name, label, **kw) -> Dict:
    basename = f'{name}_{label}'
    calculator['label'] = basename
    calculator['chk'] = basename + '.chk'
    calculator.update(kw)
    return calculator


def main():
    # get arguments
    parser = ArgumentParser()
    parser.add_argument('filename', type=Path)
    parser.add_argument('--config', default='config.ini')
    args = parser.parse_args()
    name = args.filename.stem
    # read calculator information from config file
    config = ConfigParser()
    config.read(args.config)
    calc_name = config['General']['calculator']
    calculator = dict(config['Calculator'])
    # read atoms
    atoms = read(args.filename)
    # iterate over molecules
    with workdir(name, mkdir=True):
        file_opt = Path(f'{name}_opt.log')
        if not file_opt.is_file():
            calculator = update_gaussian_kw(calculator,
                                            name=name,
                                            label='opt',
                                            charge=0,
                                            mult=1)
            calc = Gaussian(**calculator)
            opt = GaussianOptimizer(atoms, calc)
            try:
                opt.run()
            except PropertyNotImplementedError as e:
                print(f'{e}\n\nizxle: unknown optimization error\n', file=sys.stderr)
        # read optimized structure
        atoms_opt = read(file_opt)
        # iterate over charges
        for charge in {0, 1, -1}:
            # copy Atoms object
            atoms_charge = atoms_opt.copy()
            # multiplicity
            mult = 1 + (charge in {1, -1})
            # initialize calculator
            calculator = update_gaussian_kw(calculator,
                                            name=name,
                                            label=charge,
                                            charge=charge,
                                            mult=mult)
            # TODO: Check for population analysis keywords
            calc = Gaussian(**calculator)
            atoms_opt.calc = calc
            # perform calculation
            atoms_opt.get_potential_energy()


if __name__ == '__main__':
    main()
#
