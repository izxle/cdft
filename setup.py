from setuptools import setup

setup(
    name='cdft',
    keywords='cdft dft',
    version='0.0.1',
    description='Conceptual DFT',
    license='MIT',
    author='Oscar X. Guerrero Gutiérrez',
    author_email='xguerrero@cinvestav.mx',
    python_requires='>=3.7',
    install_requires=['ase', 'scikit-learn'],
    py_modules=[],
    )
